/**
 * Beschreiben Sie hier die Klasse aoc.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */

import java.io.FileReader;
import java.util.Arrays;
import com.opencsv.*;
import java.util.ArrayList;
import java.util.Iterator;


public class aoc
{
    
     // Input file d<Number>[e|i] 
    String inputFile = "";

    // Zeilenzahl der Eingabedatei
    int inputFileLines = 0;

    // Eingabe wird in eine ArrayList von String-Arrays ("input") eingelesen.
    // Das muss man je nach Eingabeformat und Aufgabenstellung sicherlich 
    // weiterverarbeiten und in eine andere Datenstruktur überführen.
    ArrayList<String[]> input = new ArrayList<String[]>();

    /**
     * Konstruktor für Objekte der Klasse aoc
     */
    public aoc() throws Exception {
       
    }

 
     /**
     * Beispielhafte Methode zum Einlesen von CSV Daten
     * in die ArrayList von String Arrays ("input").
     *  
     * Dokumentation: http://opencsv.sourceforge.net/
     **/
    public void readInput(String filename, char fieldSeperator) throws Exception {
        CSVParser myParser = new CSVParserBuilder()
            .withSeparator(fieldSeperator)  // Trennzeichen
            .build();

        CSVReader reader = new CSVReaderBuilder(new FileReader(filename))
            .withCSVParser(myParser)
            .build();

        String [] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] is an array of values from the line
            this.inputFileLines++;
            input.add(nextLine);
        }
    }

    /**
     * Kontrollausgabe der input ArrayList.
     **/
    public void printInput() {

        // Ausgabe der Input-Datei
        Iterator<String[]> iter = input.iterator();
        int lnum = 0;
        int maxFN = 0;
        int minFN = 0;

        while(iter.hasNext()) {
            String[] line=iter.next();

            int lineFNum = line.length;
            for (int i=0;i<lineFNum;i++) {
                System.out.println("[" + lnum + "][" + i + "] " + line[i] + " ");
            }
            lnum++;
        }
        System.out.println("");
        System.out.println("___ Info _______");
        System.out.println(lnum + " Zeilen");
    }

    
}

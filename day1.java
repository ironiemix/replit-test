import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Beschreiben Sie hier die Klasse day1.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class day1 extends aoc {

    // Eingabedatei (kann man hier ändern, weil das Eingeben 
    // der passenden Datei bei jeder Instaziierung lästig ist)
    // Vorshlag für die Benennung d<Tagesnummer>[e|i] 
    // e: Example für die Beispieldaten
    // i: Input für die Rätseleingabe
    String inputFile = "d1i";

    public day1() throws Exception {
        this.readInput(inputFile,'\n');
        this.printInput();
    }
    
    public int partOne() {
        int maxCalories=0;
        int packCalories=0;
        for(String line[]: input) {
            String zeilenString=line[0];

            if(!zeilenString.equals("")) {
                // Wenn es keine Leerzeile ist:
                // zu packCalories hinzuzählen, dabei 
                // muss der String in eine Integer Zahl umgewandelt werden 
                // mit Integer.parseInt();
                packCalories=packCalories+Integer.parseInt(zeilenString);
            } else {
                // Wenn es eine Leerzeile ist: 
                // Schauen, ob packCalories größer ist als maxCalories.
                // Wenn ja, merken, dann packCalories auf Null
                // zurücksetzen.
                if (packCalories > maxCalories) {
                    maxCalories = packCalories;
                }
                packCalories=0;
            }
        }

        return maxCalories;
    }

    public int partOneWithList() {
        // Diese Lösung packt die Werte für die Kalorienpakete in eine Array-List
        // Diese kann man, nmachdem man alle Pakete hinzugefügt hat sortieren, so kann man 
        // direkt das größte/kleinste Paket ermitteln. Ebenso für die drei größten. 
        
        // ArrayList für die Kalorienpakete
        ArrayList<Integer> kalorienpakete = new ArrayList<>();
        
        int aktuellesPaket=0;
        for(String[] line: input) {
            // Die Zeile besteht nur aus einem Element, line[0].
            String line0=line[0];
            // Solange keine Leerzeile auftritt, muss man die Kalorien 
            // zum aktuellen Paket addieren.
            if(!line0.equals("")) {
                aktuellesPaket += Integer.parseInt(line0);
                System.out.println(line0);
            }
            // Wenn eine Leerzeile kommt, ist das Paket fertig und kann 
            // an die ArrayList angehängt werden.
            if(line0.equals("")) { 
                System.out.println("Paket: " + aktuellesPaket);
                kalorienpakete.add(aktuellesPaket);
                aktuellesPaket=0;
            }
        }
        
        // Jetzt kann man die Liste mit Hilfe des Collection-INterfaces sortieren, so dass 
        // das 0. Element das größte ist.
        Collections.sort(kalorienpakete, Collections.reverseOrder());
        // Dieses 0.te Element holt man sich jetzt und hat die Aufgabe gelöst.
        System.out.println("Part1: " + kalorienpakete.get(0));
        return kalorienpakete.get(0);
    }

    public int partTwoWithList() {
        // Das ist einfach eine Kopie von Teil 1
        ArrayList<Integer> kalorienpakete = new ArrayList<>();
        int aktuellesPaket=0;
        for(String[] line: input) {
            String line0=line[0];
            if(!line0.equals("")) {
                aktuellesPaket += Integer.parseInt(line0);
                System.out.println(line0);
            }
            if(line0.equals("")) { 
                System.out.println("Paket: " + aktuellesPaket);
                kalorienpakete.add(aktuellesPaket);
                aktuellesPaket=0;
            }
        }
        
        Collections.sort(kalorienpakete, Collections.reverseOrder());
        // Bis hierher wie Teil 1 
        
        // Jetzt haben wir die Liste der Pakete, und zwar so, dass die
        // grö0ten Pakete das 0.te, 1.te und 2.te Element der Liste sind.
        // Die drei zählen wir zusammen, fertig.

        int summe = kalorienpakete.get(0)+kalorienpakete.get(1)+kalorienpakete.get(2);
        
        System.out.println("Part2: " + summe);
        return summe;

    }

}
